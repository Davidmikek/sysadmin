# SysAdmin

**Pri opravljanju vaje podrobno opišite vse uporabljene ukaze**

1. Na gitlab.com vmesniku opravite `fork` repozitorija [SysAdmin](https://gitlab.com/jan.popic/sysadmin), nato pa **preko ukazne** lupine prenesite **VAŠO** verzijo repozitorija na računalnik.

    1.1. Opišite razliko met `git clone` in `git fork`

2. V repozitorij dodajte (preko ukazne lupine) tekstovno datoteko ime_priimek.txt (lahko je prazna) in jo objavite na vašem git repozitoriju.

    2.1 Opišite razliko med `git commit` in `git push`

